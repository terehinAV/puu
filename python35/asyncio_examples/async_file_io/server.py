import socket
import aiofiles
import asyncio
from datetime import datetime, timezone


async def run(host=None, port=None):
    soc = socket.socket()
    if not host:
        host = "localhost"
    if not port:
        port = 12345

    soc.bind((host, port))

    soc.listen(5)
    while True:
        connection, addr = soc.accept()
        print('Got connection from', addr)
        data = connection.recv(3)
        if data in (b'end',):
            print("Got 'end' from {}. All files was received".format(addr))
            break
        elif data in (b'mp3', b'wav'):
            print("{} file received".format(data.decode('utf-8')))
            file_ext = data
        else:
            print("{} is not correct file extension".format(data.decode('utf-8')))
            connection.close()
            continue
        pack = connection.recv(4096)
        name = "./to/{}.{}".format(datetime.now(timezone.utc).astimezone().strftime("%Y_%m_%dT_%H_%M_%S_%f%z"),
                                   file_ext.decode('utf-8'))
        async with aiofiles.open(name, mode='wb') as f:
            while pack:
                await f.write(pack)
                pack = connection.recv(4096)
            f.close()
            print("Done Receiving")
            connection.close()
    soc.close()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    tasks = [loop.create_task(run())]
    wait_tasks = asyncio.wait(tasks)
    loop.run_until_complete(wait_tasks)
    # run()

