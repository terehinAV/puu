import socket
from os import listdir
import aiofiles
import asyncio
from os.path import isfile, join


async def send_file(filename, host=None, port=None):
    soc = socket.socket()
    if not host:
        host = "localhost"
    if not port:
        port = 12345
    run = True
    try:
        soc.connect((host, port))
    except Exception as e:
        print(e.args)
        run = False
    file_ext = bytes(filename[-3:], 'utf-8')
    # if file_ext not in  (b'mp3',):
    #     return
    soc.send(file_ext)
    if run:
        async with aiofiles.open(filename, mode='rb') as f:
            pack = await f.read(4096)
            try:
                while pack:
                    soc.send(pack)
                    pack = await f.read(4096)
                print("Done sending")
            except ConnectionAbortedError:
                print("Server broke connection")
                return
            finally:
                soc.shutdown(socket.SHUT_WR)
                soc.close()


def send_stop(host=None, port=None):
    soc = socket.socket()
    if not host:
        host = "localhost"
    if not port:
        port = 12345
    try:
        soc.connect((host, port))
    except Exception as e:
        print(e.args)
    file_ext = bytes("end", 'utf-8')
    soc.send(file_ext)


if __name__ == "__main__":
    only_files = [f for f in listdir("./from") if isfile(join("./from", f))]
    # for file_name in only_files:
    #     send_file('./from/{}'.format(file_name))

    loop = asyncio.get_event_loop()

    while only_files:
        task = [loop.create_task(send_file('./from/{}'.format(only_files.pop())))]
        wait_task = asyncio.wait(task)
        loop.run_until_complete(wait_task)
    send_stop()
