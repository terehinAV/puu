# -*- coding: utf-8 -*-
"""
python 3.5
"""
import asyncio
import configparser


class ConfigMonitor:
    """Класс с асинхронным методом config_monitor отслеживающим изменение файла config.ini"""
    def __init__(self, filename):
        self.fname = filename
        self.monitor_loop = asyncio.get_event_loop()
        self.config_file = None
        self.current_config = None
        self.current_config = self.load_configs(self.fname)

    @staticmethod
    def load_configs(filename):
        config_file = configparser.ConfigParser()
        config_file.read(filename)
        return config_file

    async def config_monitor(self):
        """Сопрограмма проверяющая изменение в файле конфигурации"""
        while True:
            config = self.load_configs(self.fname)
            if config and config != self.current_config:
                print("Файл изменился")
                break
            await asyncio.sleep(0)

    def loop_start(self):
        try:
            self.monitor_loop.run_until_complete(self.config_monitor())
        finally:
            self.monitor_loop.close()

if __name__ == "__main__":
    cm = ConfigMonitor('config.ini')
    cm.loop_start()


