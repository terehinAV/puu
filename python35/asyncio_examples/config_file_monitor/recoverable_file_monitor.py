# -*- coding: utf-8 -*-
"""
python 3.5
"""
import asyncio
import configparser


class ConfigMonitor:
    """
    Класс с асинхронным методом config_monitor отслеживающим изменение файла config.ini
    Блокирующий цикл loop позволяет при изменении конфигурационного файла завершать текущий задачи и
    создавать новые.
    """
    def __init__(self, filename):
        self.fname = filename
        self.monitor_loop = asyncio.get_event_loop()
        self.config_file = None
        self.current_config = None
        self.tasks = []
        self.wait_tasks = []
        self.current_config = self.load_configs(self.fname)
        self.reload = False

    def loop(self):
        try:
            while True:
                self.tasks = [self.monitor_loop.create_task(self.config_monitor())]
                """В self.tasks можно добавить другие сопрограммы с циклом по значению атрибута reload,
                например, "while not self.reload:", зависящие от актуальных значений конфигурации.
                Цикл событий будет переключаться между задачами.
                """
                self.wait_tasks = asyncio.wait(self.tasks)
                try:
                    self.monitor_loop.run_until_complete(self.wait_tasks)
                except KeyboardInterrupt as ke:
                    print(ke.args)
            self.monitor_loop.close()
        except KeyboardInterrupt as ke:
            print(ke.args)

    @staticmethod
    def load_configs(filename):
        config_file = configparser.ConfigParser()
        config_file.read(filename)
        return config_file

    async def config_monitor(self):
        self.reload = False
        """Сопрограмма проверяющая изменение в файле конфигурации"""
        print("Стартуем мониторинг конфигурационного файла.")
        while True:
            config = self.load_configs(self.fname)
            if config and config != self.current_config:
                self.current_config = config
                self.reload = True
                print("Файл изменился, останавливаем мониторинг.")
                break
            await asyncio.sleep(0)


if __name__ == "__main__":
    cm = ConfigMonitor('config.ini')
    cm.loop()


