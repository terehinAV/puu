import socket
from os import listdir
from os.path import isfile, join


def send_file(filename, host=None, port=None):
    s = socket.socket()
    if not host:
        host = "localhost"
    if not port:
        port = 12345
    run = True
    try:
        s.connect((host, port))
    except Exception as e:
        print(e.args)
        run = False
    file_ext = bytes(filename[-3:], 'utf-8')
    s.send(file_ext)
    if run:
        with open(filename, 'rb') as f:
            l = f.read(4096)
            while l:
                s.send(l)
                l = f.read(4096)
        print("Done Sending")
        s.shutdown(socket.SHUT_WR)
        s.close


def send_stop(host=None, port=None):
    s = socket.socket()
    if not host:
        host = "localhost"
    if not port:
        port = 12345
    try:
        s.connect((host, port))
    except Exception as e:
        print(e.args)
    file_ext = bytes("end", 'utf-8')
    s.send(file_ext)

if __name__ == "__main__":
    only_files = [f for f in listdir("./from") if isfile(join("./from", f))]
    while only_files:
        send_file('./from/{}'.format(only_files.pop()))
    send_stop()
