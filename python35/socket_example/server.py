import socket
from datetime import datetime, timezone


def run():
    s = socket.socket()
    host = "localhost"
    port = 12345

    s.bind((host, port))

    s.listen(5)
    while True:
        c, addr = s.accept()
        print('Got connection from', addr)
        data = c.recv(3)
        if data in (b'end',):
            print("All files was recept")
            break
        elif data not in (b'mp3',):
            continue
        l = c.recv(4096)
        name = "./to/{}.mp3".format(datetime.now(timezone.utc).astimezone().strftime("%Y_%m_%dT_%H_%M_%S_%f%z"))
        with open(name, 'wb') as f:
            while l:
                f.write(l)
                l = c.recv(4096)
            f.close()
            print("Done Receiving")
            c.close()
    s.close()
run()
