# -*- coding: utf-8 -*-
import matplotlib as mpl
import numpy as np
mpl.use('Agg')
import matplotlib.pyplot as plt


def generate_pie_chart_mpl(x_vals, axis_labels):
    # вырубаем отображение окна
    colors = ['#513B56', '#9BC53D', '#FDE74C', '#348AA7']
    plt.ioff()
    dict_data = dict(zip(axis_labels, x_vals))
    dict_data = {k: v for k, v in dict_data.items() if v}
    dict_list = sorted(dict_data.items(), key=lambda x: x[1], reverse=True)
    x_vals = [x[1] for x in dict_list]

    axis_labels = ["{} ({} р.)".format(x[0], round(float(x[1]), 2)) for x in dict_list]
    labels = axis_labels
    values = np.array(x_vals)

    fig1, ax1 = plt.subplots(figsize=(10, 7))

    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

    patches, texts, autotexts = ax1.pie(values, labels=labels, autopct="%1.1f%%", colors=colors)

    for text in texts:
        text.set_color('grey')
    for autotext in autotexts:
        autotext.set_color('grey')

    # draw circle
    centre_circle = plt.Circle((0, 0), 0.80, fc='white')
    fig = plt.gcf()
    fig.gca().add_artist(centre_circle)

    plt.title('Пример графика pie chart')

    with open("test.jpg", "wb") as f:
        plt.savefig(f, format='png', dpi=100)


if __name__ == "__main__":
    axis_labels = ["I", "II", "III"]
    x_vals = [20, 30, 40]
    generate_pie_chart_mpl(x_vals, axis_labels)
