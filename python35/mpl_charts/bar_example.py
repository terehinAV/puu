# -*- coding: utf-8 -*-
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
# mpl.use('Agg')
import matplotlib.pyplot as plt


def generate_bar_chart(dataset, members, categories):
    """
    Генерирует столбцовую диаграмму по категориям и членам группы

    dataset - список списков, строка -  1 категория,
            элемент строки - значение по этой категории у конкретного участника
    members - подписи по оси Х - участники
    categories - категории, деления в столбцах

    обязательные условия:
        len(dataset) == len(categories) - количество записей по участникам не соответствует количеству категорий
        len([dataset[i] for i in range(0, len(dataset)) if len(dataset[i]) == len(members)]) == len(dataset)
    """

    max_lim = max([sum(x) for x in zip(*dataset)])
    max_lim += max_lim/10
    mpl.rc('font', serif='Helvetica Neue')
    mpl.rc('text', usetex='false')

    fig = plt.gcf()
    fig.set_size_inches(18, 13)

    ind = np.arange(len(members))
    width = 0.4

    pp = []
    nd_dataset = [np.array(x) for x in dataset]
    colors = ['#513B56', '#9BC53D', '#FDE74C', '#348AA7']
    for n, ds in enumerate(dataset):
        _dataset = nd_dataset[:n]
        if _dataset:
            bottom = sum(_dataset)
            pp.append(plt.bar(ind, nd_dataset[n], width, bottom=bottom,  color=colors[n]))
        else:
            pp.append(plt.bar(ind, nd_dataset[n], width, color=colors[n]))

    plt.ylim([0, max_lim])
    plt.yticks(fontsize=12)
    plt.ylabel("Сумма", fontsize=12)
    plt.xticks(ind, members, fontsize=12, rotation=90)
    plt.xlabel('Участники', fontsize=12)

    ax = plt.gca()
    ax.set_xlim(-1, len(members))

    lgd = plt.legend(tuple(pp),
                     tuple(categories),
                     fontsize=12,
                     ncol=1,
                     framealpha=0,
                     fancybox=True,
                     bbox_to_anchor=(1.04, 0.5),
                     loc="center left",
                     borderaxespad=0,
                     prop={'size': 20})
    plt.title('Пример графика stacked bar', fontsize=16)

    with open("test.jpg", "wb") as f:
        plt.savefig(f, format='png', dpi=100, additional_artists=(lgd,), bbox_inches="tight")
    # img = None
    # if plt:
    #     buf = io.BytesIO()
    #     plt.savefig(buf, format='png', dpi=100, additional_artists=(lgd,), bbox_inches="tight")
    #     img = buf.getvalue()
    #     plt.close('all')
    #     buf.seek(0)
    #     buf.close()
    # if img:
    #     return img

if __name__ == "__main__":
    dataset = [
        (1, 2, 3),
        (3, 4, 5),
        (6, 7, 8),
        (9, 10, 11)
    ]
    headers = ["I", "II", "III"]
    categories = ["one", "two", "three", "four"]
    generate_bar_chart(dataset, headers, categories)